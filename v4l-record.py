#!/usr/bin/python
# coding: utf-8

import os
import gi
import pipes
import re

gi.require_version('Gtk', '3.0')
gi.require_version('Vte', '2.91')

from gi.repository import Gtk, GObject, Vte, GLib


class ScrollableTerminal(Gtk.Box):
    __gsignals__ = {
        'new-line': (GObject.SIGNAL_RUN_FIRST, None,
                        (str,))
    }

    def __init__(self):
        Gtk.Box.__init__(self, spacing=0)

        self.last_logged_line = 'Veeery weird liiiine'

        self.terminal = Vte.Terminal()
        self.terminal.spawn_sync(
            Vte.PtyFlags.DEFAULT, #default is fine
            os.environ['HOME'], #where to start the command?
            ["/bin/sh"], #where is the emulator?
            [], #it's ok to leave this list empty
            GLib.SpawnFlags.DO_NOT_REAP_CHILD,
            None, #at least None is required
            None,
            )
        self.terminal.set_size(130, 10)
        self.terminal.set_can_focus(False)
        self.terminal.connect('contents-changed', self.emit_last_line)

        self.scrollbar = Gtk.Scrollbar(orientation=Gtk.Orientation.VERTICAL,
                                       adjustment=self.terminal.get_vadjustment())        

        self.pack_start(self.terminal, True, True, 0)
        self.pack_start(self.scrollbar, False, False, 0)
        
    def run_command(self, command):
        self.terminal.feed_child(command + '\n', len(command) + 1)

    def emit_last_line(self, terminal):
        column,row = self.terminal.get_cursor_position()
        terminal_text = self.terminal.get_text_range(row-5, 0, row, column, lambda x, y, z: True)[0]
        terminal_lines = []
        for line in reversed(terminal_text.split('\\\\n')):
            if line == self.last_logged_line:
                break
            if line:
                terminal_lines.append(line)
        
        if terminal_lines:
            self.last_logged_line = terminal_lines[-1]
        
        for line in reversed(terminal_lines):
            self.emit('new-line', line.strip())



# terminal with progress bar        
class TerminalWithProgressBar(Gtk.Box):

    def __init__(self):
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.VERTICAL, spacing=0)
        
        self.progress = Gtk.ProgressBar()
        self.terminal = ScrollableTerminal()

        self.terminal.connect('new-line', self.update_progress)
        
        self.translate = lambda x: 0        

        self.pack_start(self.progress, False, True, 10)
        self.pack_start(self.terminal, True, True, 10)

    def run_command(self, command, translate=lambda x: 0):
        self.translate = translate
        self.progress.set_fraction(0.0)
        self.terminal.run_command(command)
    
    def update_progress(self, obj, line):
        value = self.translate(line)
        if value:
            self.progress.set_fraction(value)

    def reset_progress(self):
        self.progress.set_fraction(0.0)
       
 
class MainWindow(Gtk.Window):

    def __init__(self):
        Gtk.Window.__init__(self, title='Záznam videa z VHS')
        self.set_size_request(300, 300)
        self.connect('delete-event', lambda w,e : Gtk.main_quit())

        self.running = False
        self.duration = '04:00:10'

        self.vbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=6)
        self.vbox.set_margin_right(20)
        self.vbox.set_margin_left(20)
        self.vbox.set_margin_bottom(20)
        self.vbox.set_margin_top(20)
        
        self.grid = Gtk.Grid()
        self.grid.set_column_spacing(10)
        self.open_button = Gtk.Button(label=u'Otevřít')
        self.open_button.connect('clicked', self.on_open_clicked)
        self.open_label = Gtk.Label(u'Cílová složka:')
        self.open_label.set_halign(Gtk.Align.START)
        self.open_entry = Gtk.Entry()
        self.open_entry.set_width_chars(60)
        self.open_entry.set_hexpand(True)
        self.open_entry.set_text(os.environ['HOME'])
        
        self.duration_entry = Gtk.Entry()
        self.duration_entry.set_text(self.duration)
        self.duration_label = Gtk.Label(u'Doba nahrávání:')
        self.duration_label.set_halign(Gtk.Align.START)
        self.duration_label.set_margin_right(20)

        self.start_button = Gtk.Button(label=u'Spustit nahrávání')
        self.start_button.set_margin_left(30)
        self.start_button.connect('clicked', self.start_encoding)

        self.cancel_button = Gtk.Button(label=u'Zrušit nahrávání')
        self.cancel_button.set_margin_left(30)
        self.cancel_button.connect('clicked', self.cancel_encoding)

        self.grid.add(self.open_label)
        self.grid.attach(self.open_entry, 1, 0, 1, 1)
        self.grid.attach(self.open_button, 2, 0, 1, 1)

        self.grid.attach(self.duration_label, 0, 1, 1, 1)
        self.grid.attach(self.duration_entry, 1, 1, 1, 1)

        self.grid.attach(self.start_button, 3, 0, 1,2)
        self.grid.attach(self.cancel_button, 3, 0, 1,2)

        self.terminal = TerminalWithProgressBar()
        self.terminal.hide()        
        
        self.vbox.pack_start(self.grid, False, False, 0)
        self.vbox.pack_start(self.terminal, True, True, 0)
        self.add(self.vbox)

    def cancel_encoding(self, widget):
        self.terminal.run_command('q')
        self.encoding_ended()

    def start_encoding(self, widget):
        self.terminal.run_command('clear')
        self.terminal.run_command('{}/v4l-capture --encode {} {}'.format(
            os.path.dirname(os.path.realpath(__file__)),
            pipes.quote(self.open_entry.get_text()), self.duration_entry.get_text()),
            self.percent_progress)
        self.running = True
        self.start_button.hide()
        self.cancel_button.show()
        
    def encoding_ended(self):
        self.running = False
        self.terminal.reset_progress()
        self.cancel_button.hide()
        self.start_button.show()
        

    def percent_progress(self, line):
        m = re.search(' ([0-9]{1,2})% ', line)
        #print line
        if m:
            return float(m.group(1))/100 
        if 'finished' in line:
            self.encoding_ended()
            return 1.0

    def on_open_clicked(self, widget):
        dialog = Gtk.FileChooserDialog('Vyberte cílovou složku', self,
            Gtk.FileChooserAction.SELECT_FOLDER,
            (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
             Gtk.STOCK_OPEN, Gtk.ResponseType.OK))

        response = dialog.run()
        if response == Gtk.ResponseType.OK:
            self.open_entry.set_text(dialog.get_filename())

        dialog.destroy()

def main():
    win=MainWindow()
    win.show_all()
    Gtk.main()


if __name__=='__main__': main()
